package nuitinfo.beebeez;

/**
 * Created by Sébastien on 04/12/2015.
 */
public class Grille {
    static int tab[][] = {
            {6,7,1,1,1,1,1,1,1,1},
            {8,9,14,0,0,0,0,0,13,1},
            {1,2,17,1,1,1,1,1,11,1},
            {1,1,11,1,14,3,13,1,11,1},
            {1,1,16,0,12,1,11,1,11,1},
            {1,1,11,1,1,1,16,0,12,1},
            {0,0,17,1,1,1,11,1,1,1},
            {1,1,16,0,13,1,15,0,13,1},
            {1,1,11,1,11,1,1,1,11,1},
            {10,0,12,1,15,0,1,1,4,1}};
    public static int decript(int tab[][], int x, int y){
        switch(tab[x][y]){
            case 0:
                return R.drawable.route1;

            case 1:
                return R.drawable.mur1;

            case 2:
                return R.drawable.abeillecombat;

            case 3:
                return R.drawable.bourdon2;

            case 4:
                return R.drawable.abeilleemprisonner;
            case 5:
                return R.drawable.bebeabeille;

            case 6:
                return R.drawable.r6r;

            case 7:
                return R.drawable.r7r;

            case 8:
                return R.drawable.r8r;

            case 9:
                return R.drawable.r9r;

            case 10:
                return R.drawable.clef1;
            case 11:
                return R.drawable.route;
            case 12:
                return R.drawable.routecoude;
            case 13:
                return R.drawable.routecoude1;
            case 14:
                return R.drawable.routecoude2;
            case 15:
                return R.drawable.routecoude3;
            case 16:
                return R.drawable.routet;
            case 17:
                return R.drawable.routet1;
        }
        return 0;
    }
    static int[][] copy(){
        int t[][]=
                {{6,7,1,1,1,1,1,1,1,1},
                        {8,9,14,0,0,0,0,0,13,1},
                        {1,2,17,1,1,1,1,1,11,1},
                        {1,1,11,1,14,0,13,1,11,1},
                        {1,1,16,0,12,1,11,1,11,1},
                        {1,1,11,1,1,1,16,0,12,1},
                        {0,0,17,1,1,1,11,1,1,1},
                        {1,1,16,0,13,1,15,0,13,1},
                        {1,1,11,1,11,1,1,1,11,1},
                        {10,0,12,1,15,0,1,1,4,1}};
        return t;
    }
}
