package nuitinfo.beebeez;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

/**
 * Created by Sébastien on 03/12/2015.
 */
public class Maps {
    private int map[][];
    private int mapOld[][];
    private int xStart;
    private int yStart;
    private int xEnd;
    private int yEnd;

    private  int posXAbeille;
    private int posYAbeille;

    private int posXClef;
    private int posYClef;
    private int posXAPrison;
    private int posYPrison;
    private int rucheX;
    private int rucheY;
    private boolean haveClef;
    private boolean haveSuivi;


    private int posXGuepe;
    private int posYGuepe;
    public Maps(){
        map= Grille.tab.clone();
        mapOld = Grille.copy();
        haveClef = false;
        haveSuivi = false;
        for (int x = 0; x < map.length; x++) {
            for (int y = 0; y < map[0].length; y++) {
                if (map[x][y] == 2){
                    posXAbeille = x;
                    posYAbeille = y;
                    mapOld[x][y]=0;
                }
                if (map[x][y] ==3){
                    posXGuepe = x;
                    posYGuepe = y;
                    mapOld[x][y]=0;
                }
                if (map[x][y] == 10){
                    posXClef = x;
                    posYClef = y;
                }
                if (map[x][y] ==9){
                    rucheX = x;
                    rucheY = y;
                }
                if (map[x][y] == 4){
                    posXAPrison = x;
                    posYPrison= y;
                    mapOld[x][y] = 11;
                }
            }
        }
    }

    public int[][] getMap() {
        return map;
    }
    public void setMap(int[][] map) {
        map = map;
    }
    public int getxStart() {
        return xStart;
    }
    public void setxStart(int xStart) {
        this.xStart = xStart;
    }
    public int getyStart() {
        return yStart;
    }
    public void setyStart(int yStart) {
        this.yStart = yStart;
    }
    public int getxEnd() {
        return xEnd;
    }
    public void setxEnd(int xEnd) {
        this.xEnd = xEnd;
    }
    public int getyEnd() {
        return yEnd;
    }
    public void setyEnd(int yEnd) {
        this.yEnd = yEnd;
    }


    public Bitmap createImage(Resources r){
        Bitmap result = BitmapFactory.decodeResource(r, R.drawable.fond1);
        Bitmap m = result.copy(Bitmap.Config.ARGB_8888, true);
        int posx=0;
        int posy=0;
        int largeur=0;
        int hauteur=0;
        //mapOld= map.clone();
        for (int xResult = 0; xResult < 10; xResult++) {
            for (int yResult = 0; yResult < 10; yResult++) {
                Bitmap es = BitmapFactory.decodeResource(r, Grille.decript(Grille.tab, xResult, yResult));
                largeur=es.getWidth();
                hauteur=es.getHeight();
                for (int x = 0 ; x < largeur-1; x++)
                    for (int y=0; y < hauteur-1; y++) {
                        if (posx+x < m.getWidth() || posy+y < m.getHeight() && (posx+x < m.getWidth() || posy+y < m.getHeight()))
                            m.setPixel(posx+x, posy+y, es.getPixel(x, y));
                    }
                posx += es.getWidth()-1;
            }
            posx=0;
            posy += largeur-1;
        }
        return m;
    }

    public Bitmap updateImage(Resources r){
        Bitmap result = BitmapFactory.decodeResource(r, R.drawable.fond1);
        Bitmap m = result.copy(Bitmap.Config.ARGB_8888, true);
        int posx=0;
        int posy=0;
        int largeur=0;
        int hauteur=0;
       // mapOld= map.clone();
        for (int xResult = 0; xResult < 10; xResult++) {
            for (int yResult = 0; yResult < 10; yResult++) {
                Bitmap es = BitmapFactory.decodeResource(r, Grille.decript(map, xResult, yResult));
                largeur=es.getWidth();
                hauteur=es.getHeight();
                for (int x = 0 ; x < largeur-1; x++)
                    for (int y=0; y < hauteur-1; y++) {
                        if (posx+x < m.getWidth() || posy+y < m.getHeight())
                            m.setPixel(posx+x, posy+y, es.getPixel(x, y));
                    }
                posx += es.getWidth()-1 ;
            }
            posx=0;
            posy += largeur-1;
        }
        return m;
    }

    public void clef(){
        if (posXAbeille == posXClef && posYAbeille == posYClef) {
            map[posXAPrison][posYPrison] = 5;
            //mapOld[posXClef][posYClef] = 5;
            haveClef=true;
        }

    }
    public boolean win(){
        return posXAPrison == rucheX && posYPrison==rucheY;
    }

    public void suivi(int posX, int posY){
        if (haveClef){
            if (haveSuivi) {
                map[posXAPrison][posYPrison] = mapOld[posXAPrison][posYPrison];
                posYPrison = posY;
                posXAPrison = posX;
                map[posX][posY] = 5;
            }
            else{
                if (posXAbeille == posXAPrison && posYAbeille == posYPrison)
                    haveSuivi = true;
                }
        }
    }

    public void moveUp(){
        if (posXAbeille  <= 0 || map[posXAbeille-1][posYAbeille] == 1){
            return;
        }
        clef();
        map[posXAbeille][posYAbeille]= mapOld[posXAbeille][posYAbeille];
        map[posXAbeille-1][posYAbeille]=2;
        suivi(posXAbeille,posYAbeille);
        posXAbeille-=1;
    }

    public void moveDown(){
        if (posXAbeille >= map.length-1 || map[posXAbeille+1][posYAbeille] == 1){
            return;
        }
        clef();

        map[posXAbeille][posYAbeille]= mapOld[posXAbeille][posYAbeille];
        map[posXAbeille+1][posYAbeille]=2;
        suivi(posXAbeille,posYAbeille);

        posXAbeille+=1;
    }
    public void moveRight(){
        if (posYAbeille >= map.length-1 || map[posXAbeille][posYAbeille+1] == 1){
            return;
        }
        clef();
        map[posXAbeille][posYAbeille]= mapOld[posXAbeille][posYAbeille];
        map[posXAbeille][posYAbeille+1]=2;
        suivi(posXAbeille,posYAbeille);

        posYAbeille+=1;
    }
    public void moveLeft(){
        if (posYAbeille <= 0 || map[posXAbeille][posYAbeille-1] == 1){
            return;
        }
        clef();
        map[posXAbeille][posYAbeille]= mapOld[posXAbeille][posYAbeille];
        map[posXAbeille][posYAbeille-1]=2;
        suivi(posXAbeille,posYAbeille);

        posYAbeille-=1;
    }

    public boolean gameOver(){
    return posXGuepe == posXAbeille && posYGuepe == posYAbeille;
    }

    public void moveGuepe() {
        if (posXGuepe <= 0 || map[posXGuepe - 1][posYGuepe] == 1) {
            if (posXGuepe >= map.length - 1 || map[posXGuepe + 1][posYGuepe] == 1) {
                if (posYGuepe >= map.length - 1 || map[posXGuepe][posYGuepe + 1] == 1) {
                    map[posXGuepe][posYGuepe] = mapOld[posXGuepe][posYGuepe];
                    map[posXGuepe][posYGuepe - 1] = 3;

                    posYGuepe -= 1;
                } else {
                    map[posXGuepe][posYGuepe] = mapOld[posXGuepe][posYGuepe];
                    map[posXGuepe][posYGuepe + 1] = 3;
                    posYGuepe += 1;
                }
            } else {
                map[posXGuepe][posYGuepe] = mapOld[posXGuepe][posYGuepe];
                map[posXGuepe + 1][posYGuepe] = 3;
                posXGuepe += 1;
            }
        } else {
            map[posXGuepe][posYGuepe] = mapOld[posXGuepe][posYGuepe];
            map[posXGuepe - 1][posYGuepe] = 3;
            posXGuepe -= 1;
        }
    }

}


