package nuitinfo.beebeez;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by Sébastien on 03/12/2015.
 */
public class Game extends AppCompatActivity{
    Button up;
    Button down;
    Button left;
    Button right;
    ImageView map;
    TextView win;
    Maps m;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        Intent intent = getIntent();
        up = (Button) findViewById(R.id.up);
        up.setOnClickListener(buttonUp);

        down = (Button) findViewById(R.id.down);
        down.setOnClickListener(buttonDown);

        left = (Button) findViewById(R.id.left);
        left.setOnClickListener(buttonLeft);

        right = (Button) findViewById(R.id.right);
        right.setOnClickListener(buttonRight);

        map = (ImageView) findViewById(R.id.maps);
        m = new Maps();

        Bitmap font = m.createImage(getResources());
        map.setImageBitmap(font);
        win = (TextView) findViewById(R.id.win);
        win.setVisibility(View.INVISIBLE);

    }
    private View.OnClickListener buttonUp = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            m.moveGuepe();
            m.moveUp();
            Bitmap t = m.updateImage(getResources());
            map.setImageBitmap(t);
            if(m.gameOver()) {
                win.setText("LOSE");
                win.setVisibility(View.VISIBLE);
            }
            if (m.win())
                win.setVisibility(View.VISIBLE);

        }
    };
    private View.OnClickListener buttonDown = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            m.moveGuepe();
            m.moveDown();

            Bitmap t = m.createImage(getResources());
            map.setImageBitmap(t);
            if(m.gameOver()){
                win.setText("LOSE");
                win.setVisibility(View.VISIBLE);
            }
            if (m.win())
                win.setVisibility(View.VISIBLE);

        }
    };
    private View.OnClickListener buttonLeft = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            m.moveGuepe();
            m.moveLeft();

            Bitmap t = m.createImage(getResources());
            map.setImageBitmap(t);
            if(m.gameOver()) {
                win.setText("LOSE");
                win.setVisibility(View.VISIBLE);
            }
            if (m.win())
                win.setVisibility(View.VISIBLE);


        }
    };
    private View.OnClickListener buttonRight = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            m.moveGuepe();
            m.moveRight();
            Bitmap t = m.createImage(getResources());
            map.setImageBitmap(t);
            if(m.gameOver()) {
                win.setText("LOSE");
                win.setVisibility(View.VISIBLE);
            }
            if (m.win())
                win.setVisibility(View.VISIBLE);

        }
    };

}
